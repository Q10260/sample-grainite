package org.sample.food_app;

public class Constants {

    public static String APP_NAME = "food_app";

    public static String ORDERS_TOPIC = "orders_topic";

    public static String ORDERS_TABLE = "orders_table";
    
    public static String HANDLE_WORD_EVENTS_ACTION = "handleWordEvent";
    
    public static String WORDS_TABLE = "word_stats_table";

    public static String HANDLE_ORDERS_ACTION = "handleOrders";

    public static String ORDER_UPDATES_SUBSCRIPTION = "orderUpdates";
}