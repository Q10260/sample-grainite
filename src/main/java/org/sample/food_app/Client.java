package org.sample.food_app;

import com.grainite.api.Event;
import com.grainite.api.Grain;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.Key;
import com.grainite.api.Table;
import com.grainite.api.Topic;
import com.grainite.api.Value;
import com.grainite.api.context.GrainContext.GrainOp;

public class Client {

	public static void main(String[] args) {
		Grainite client = GrainiteClient.getClient("localhost", 5056);
		Table table = client.getTable(Constants.APP_NAME,
				Constants.WORDS_TABLE);
		Grain grain = table.getGrain(Key.of("value7"));
		System.out.println(grain);
		grain.setValue(Value.of("Quality"));

		Grain grain1 = table.getGrain(Key.of("long13426"));
		Value values = grain1.getValue();

		Grain grain2 = table.getGrain(Key.of("z"));
		grain2.setValue(Value.of(values));

		// String cmd = args[0];
		// String key = args[1];
		// String value = args[2];

		// switch (cmd.toLowerCase()) {
		// case "load":
		// new Client().load(client, key, value);
		// break;
		// }

		// client.close();
		// }

		// private void load(Grainite client, String key, String value) {
		// Topic topic = client.getTopic(Constants.APP_NAME, Constants.ORDERS_TOPIC);
		// Event topicEvent = new Event(Key.of(key), Value.of(value));
		// topic.append(topicEvent);
		// }
	}
}