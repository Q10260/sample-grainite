package org.sample.food_app.actions;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;
import com.grainite.api.context.Action.GrainRequest;

public class WordStatsHandler {

	public ActionResult handleWordEvent(Action action, GrainContext context) {
		Value word = ((GrainRequest)action).getPayload();
	    context.getLogger().info("WordStatsHandler: Received word - " +
	                             word.asString());
	    context.mapPut(0, Value.of(word.asString().substring(0, 2)+ "++++"), Value.of(context.mapGet(0, word).asLong(0)));
	    context.setValue(word);
        return ActionResult.success(action);
    }

	
}
