package org.sample.food_app.actions;

import org.sample.food_app.Constants;

import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.Key;
import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.TopicEvent;
import com.grainite.api.context.Action.GrainRequest;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;
import com.grainite.api.context.GrainContext.GrainOp;
import com.grainite.client.internal.ClientInternalProto.GetSnapshotLsnRequest.Table;

public class OrdersHandler {

    /**
     * Handle an event received by this Grain.
     *
     * Subscriptions:
     *
     * 1. orderUpdates
     * Topic Name: orders_topic
     * Topic Key: order_id
     */
    public ActionResult handleOrders(Action action, GrainContext context) {
        context.counter("handleOrders").inc();
        String asString = "";
        try {
            asString = ((TopicEvent) action).getPayload().asString();
        } catch (ClassCastException e) {
            asString = ((GrainRequest) action).getPayload().asString();
        }
        String asString1 = context.getValue().asString();
        String asString2 = context.getValue().of(Key.of("d")).asString();
        System.out.println(asString);
        System.out.println(asString2);

        GrainOp.Invoke invoke = new GrainOp.Invoke(Constants.HANDLE_WORD_EVENTS_ACTION, Value.of(asString));
        context.sendToGrain(Constants.WORDS_TABLE, Value.of(asString), invoke, null);

        // context.sendToGrain(Constants.WORDS_TABLE, Key.of("value9"),
        // new GrainContext.GrainOp.Invoke(Constants.HANDLE_WORD_EVENTS_ACTION,
        // Value.of(asString2)), null);

        // GrainOp.Invoke invoke = new GrainOp.Invoke(Constants.HANDLE_ORDERS_ACTION,
        // Value.of(asString));
        // context.sendToGrain(Constants.ORDERS_TABLE, Key.of(asString), invoke, null);
        //
        context.getLogger().info("action  == " + action.toString() + "context===" + context);
        context.getLogger().info("action=====" + asString + "contextasString===" + asString1);
        context.getLogger().info("invoke  == " + invoke.getPayload() + " " + invoke.getAction());

        return ActionResult.success(action);
    }
}
